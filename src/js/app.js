import $ from 'jquery';
import 'bootstrap';
import Rellax from 'rellax'

var rellax = new Rellax('.rellax');

let script = document.createElement('script');
script.src = `https://maps.googleapis.com/maps/api/js?key=AIzaSyAB3fgJ1iCeyCXLCiSNnGuFACPPnXuoArs&callback=initMap`;
script.defer = true;
script.async = true;

window.initMap = function(){
  let TRAVELFARCOMPANY = {lat:-37.8713,lng: 145.0190}
  let map;
  map = new google.maps.Map(document.getElementById('map'), {
    center:TRAVELFARCOMPANY,
    zoom: 12
  });

  var marker = new google.maps.Marker({
    position: TRAVELFARCOMPANY,
    map: map,
  animation:google.maps.Animation.DROP,
});
}

document.head.appendChild(script);

window.$ = $;
window.jQuery = $;
